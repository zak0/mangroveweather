package jaaska.jaakko.mangroveweather;

/**
 * Interface for passing a callback to a dialog.
 *
 * This callback is supposed to be called after the dialog is dismissed with a positive
 * user input.
 *
 * Created by jaakko on 07/12/2017.
 */

public interface PositiveDialogAction {
    void onPositiveAction();
}
