package jaaska.jaakko.mangroveweather;

/**
 * Container for user settings.
 *
 * Created by jaakko on 07/12/2017.
 */

class Settings {

    static final int SETTING_LOCATION = 0;
    static final int SETTING_UNITS = 1;
    static final int SETTING_DAYS = 2;

    /** The location the weather is requested for */
    private static String sLocation;

    /** Temperature units to use */
    private static String sUnits;

    /** For how many days the forecast is requested for */
    private static int sDays;

    static String getLocation() {
        return sLocation;
    }

    static String getUnits() {
        return sUnits;
    }

    static int getDays() {
        return sDays;
    }

    static void setLocation(String location) {
        sLocation = location;
    }

    static void setUnits(String units) {
        sUnits = units;
    }

    static void setDays(int days) {
        sDays = days;
    }

}
