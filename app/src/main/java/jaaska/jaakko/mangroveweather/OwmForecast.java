package jaaska.jaakko.mangroveweather;

import java.util.Comparator;

/**
 * A container for weather forecast data for a given day.
 *
 * Created by jaakko on 06/12/2017.
 */

public class OwmForecast implements Comparator {
    private long mDate;
    private double mTemp;
    private double mLowTemp;
    private double mHighTemp;
    private WeatherState mWeatherState;

    OwmForecast(long date, double temp, WeatherState weatherState) {
        mDate = date;
        mTemp = temp;
        mWeatherState = weatherState;

        // Low and high temperatures are updated when post-processing the data received
        // from the OWM API. This is why at the time of construction they are set to Double.MIN_VALUE.
        mLowTemp = Double.MIN_VALUE;
        mHighTemp = Double.MIN_VALUE;
    }

    void setLowTemp(double lowTemp) {
        mLowTemp = lowTemp;
    }

    void setHighTemp(double highTemp) {
        mHighTemp = highTemp;
    }

    long getDate() {
        return mDate;
    }

    double getTemp() {
        return mTemp;
    }

    double getLowTemp() {
        return mLowTemp;
    }

    double getHighTemp() {
        return mHighTemp;
    }

    WeatherState getWeatherState() {
        return mWeatherState;
    }

    enum WeatherState {
        SUN,
        STORM,
        CLOUDS,
        RAIN
    }

    /**
     * Custom comparator to sort the forecast objects by date.
     * @param o1 an OwmForecast object
     * @param o2 an OwmForecast object
     * @return a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater than the second
     */
    @Override
    public int compare(Object o1, Object o2) {
        OwmForecast first = (OwmForecast) o1;
        OwmForecast second = (OwmForecast) o2;

        if (first.getDate() > second.getDate()) {
            return 1;
        } else if (first.getDate() < second.getDate()) {
            return -1;
        }

        return 0;
    }
}
