package jaaska.jaakko.mangroveweather;

import android.app.Application;
import android.content.Context;

/**
 * Reason for this existing is to provide access to app context from anywhere in the app.
 *
 * Created by jaakko on 05/12/2017.
 */

public class MangroveWeatherApp extends Application {
    private static MangroveWeatherApp sInstance;

    public static Context getContext() {
        return sInstance;
    }

    @Override
    public void onCreate() {
        sInstance = this;
        super.onCreate();
    }
}
