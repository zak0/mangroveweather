package jaaska.jaakko.mangroveweather;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;

/**
 * A dialog for setting each of the three settings for the app.
 *
 * Created by jaakko on 07/12/2017.
 */

class SettingDialog extends Dialog {

    private PositiveDialogAction mCallBack;

    SettingDialog(@NonNull Context context, int setting, PositiveDialogAction callback) {
        super(context);

        mCallBack = callback;
        switch (setting) {
            case Settings.SETTING_LOCATION:
                initLocationSettingDialog();
                break;
            case Settings.SETTING_UNITS:
                initUnitsSettingDialog();
                break;
            case Settings.SETTING_DAYS:
                initDaysSettingDialog();
                break;
        }

        findViewById(R.id.buttonCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void initLocationSettingDialog() {
        setContentView(R.layout.dialog_location);
        findViewById(R.id.buttonDone).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editTextLocation = findViewById(R.id.editTextLocation);
                String newLocation = editTextLocation.getText().toString();
                Settings.setLocation(newLocation);
                mCallBack.onPositiveAction();
                dismiss();
            }
        });
    }

    private void initUnitsSettingDialog() {
        setContentView(R.layout.dialog_units);
        findViewById(R.id.buttonDone).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioGroup radioGroup = findViewById(R.id.radioGroupUnit);
                int checkedId = radioGroup.getCheckedRadioButtonId();

                switch (checkedId) {
                    case R.id.radioButtonMetric:
                        Settings.setUnits("metric");
                        break;
                    case R.id.radioButtonImperial:
                        Settings.setUnits("imperial");
                        break;
                }

                mCallBack.onPositiveAction();
                dismiss();
            }
        });
    }

    private void initDaysSettingDialog() {
        setContentView(R.layout.dialog_days);
        findViewById(R.id.buttonDone).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editTextDays = findViewById(R.id.editTextDays);
                String daysString = editTextDays.getText().toString();

                int days = 5;
                try {
                    days = Integer.parseInt(daysString);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }

                // Number of days must be between 1 and 5.
                days = days < 1 ? 1 : days;
                days = days > 5 ? 5 : days;

                Settings.setDays(days);

                mCallBack.onPositiveAction();
                dismiss();
            }
        });
    }
}
