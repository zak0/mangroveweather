package jaaska.jaakko.mangroveweather;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

/**
 * Activity for displaying info about the app.
 *
 * Created by jaakko on 06/12/2017.
 */

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) { // It should never be null, but Studio gives a warning without the check.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        setTitle(R.string.about);

        TextView versionView = findViewById(R.id.textViewVersion);
        TextView scrollingVersionView = findViewById(R.id.textViewScrollingVersion);
        String versionString = getResources().getString(R.string.app_version,
                BuildConfig.VERSION_NAME);

        // This is done for two views. The first one is the static one that is shown on the bottom
        // of the screen, if there's room. The second one scrolls along the about text in case the,
        // static one does not fit on the screen.
        versionView.setText(versionString);
        scrollingVersionView.setText(versionString);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        ScrollView scrollView = findViewById(R.id.scrollViewAbout);
        LinearLayout layout = findViewById(R.id.layoutAboutContent);
        LinearLayout layoutVersion = findViewById(R.id.layoutVersion);
        LinearLayout layoutScrollingVersion = findViewById(R.id.layoutScrollingVersion);

        // Scroll the version number along the scroll view if it doesn't nicely fit to the screen
        // to be on the bottom.
        //
        // Do some math to determine if it fits or not, and then hide and show either the scrolling
        // or static version view.
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) layoutScrollingVersion.getLayoutParams();
        int scrollContentHeight = layout.getHeight() + layoutVersion.getHeight()
                - layoutScrollingVersion.getHeight() - params.topMargin;


        if (scrollContentHeight > scrollView.getHeight()) {
            layoutVersion.setVisibility(View.GONE);
            layoutScrollingVersion.setVisibility(View.VISIBLE);
        } else {
            layoutVersion.setVisibility(View.VISIBLE);
            layoutScrollingVersion.setVisibility(View.GONE);
        }
    }

}
