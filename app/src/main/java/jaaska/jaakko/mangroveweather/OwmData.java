package jaaska.jaakko.mangroveweather;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Class that represents a forecast result that's received from the OWM API.
 *
 * Created by jaakko on 06/12/2017.
 */

class OwmData {
    private static final String TAG = "OwmData";

    private String mCity;
    private List<OwmForecast> mForecasts;

    /**
     * Parses the forecast from the JSON response that is received from the OWM API.
     *
     * @param jsonResponse JSON response as a String
     * @throws JSONException in case parsing the JSON fails
     */
    OwmData(String jsonResponse) throws JSONException {
        JSONObject root = new JSONObject(jsonResponse);
        mForecasts = new ArrayList<>();

        // Get the location
        mCity = root.getJSONObject("city").getString("name");
        Log.d(TAG, "constructor() - city = '" + mCity + "'");

        // Then parse the actual forecast data.
        JSONArray forecasts = root.getJSONArray("list");
        for (int i = 0; i < forecasts.length(); i++) {
            JSONObject forecast = forecasts.getJSONObject(i);

            Log.d(TAG, "constructor() - forecast = '" + forecast.toString() + "'");

            OwmForecast parsedForecast = parseForecast(forecast);
            if (parsedForecast != null) {
                Log.d(TAG, "constructor() - adding a forecast to array");
                mForecasts.add(parseForecast(forecast));
            }
        }

        // Now when the data is in our format, we can post-process it to the form that we wont.
        // I.e. to have only one forecast entry per day, containing the daily minimum and
        // maximum temperatures.
        postProcessForecasts();
    }

    /**
     * Post-process the forecast data into the form that we wont.
     * I.e. to have only one forecast entry per day, containing the daily minimum and
     * maximum temperatures.
     *
     * The weather state will be the one in the first forecast object for eact day.
     *
     * After this method finished, the mForecasts List contains only one OwmDate object per day,
     * with daily minimum and maximum temperatures.
     */
    private void postProcessForecasts() {
        if (mForecasts == null) {
            Log.d(TAG, "postProcessForecasts() - mForecasts was null! returning...");
            return;
        }

        List<OwmForecast> processedForecasts = new ArrayList<>();

        int dayInProcess = Integer.MIN_VALUE; // current day that is being processed
        double dailyMin = Double.MAX_VALUE; // minimum temp for the day being processed
        double dailyMax = Double.MIN_VALUE; // maximum temp for the day being processed
        OwmForecast dailyForecast = null; // the forecast for the day that's being processed

        // Iterate through all the forecasts and get the min and max temperatures for each day.
        for (OwmForecast f : mForecasts) {
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(f.getDate());
            int day = cal.get(Calendar.DAY_OF_YEAR);

            if (dayInProcess == Integer.MIN_VALUE) {
                // This is the very first forecast that we're handling.
                dayInProcess = day;
                dailyForecast = f; // For the today's weather (or the "now" weather) we always use
                                    // the first forecast in the array, no matter at what time of day
                                    // it is.
                dailyMin = f.getTemp();
                dailyMax = f.getTemp();
            } else if (day != dayInProcess) {
                // We moved to the next day's forecasts.
                // Store the current one being processed, and start handling the next one.
                dailyForecast.setLowTemp(dailyMin);
                dailyForecast.setHighTemp(dailyMax);
                processedForecasts.add(dailyForecast);
                dailyForecast = f; // Take this forecast to be the one for this day.
                dayInProcess = day;
                dailyMin = f.getTemp();
                dailyMax = f.getTemp();
            } else {
                // We're handling an another forecast for the dayt that's currently being processed.
                if (f.getTemp() < dailyMin) {
                    dailyMin = f.getTemp();
                }
                if (f.getTemp() > dailyMax) {
                    dailyMax = f.getTemp();
                }
            }
        }

        mForecasts = processedForecasts;
        Log.d(TAG, "postProcessForecasts() - size after processing = " + mForecasts.size());

    }

    /**
     * Parses a JSONObject containing the weather at the three hour slot.
     *
     * @param jsonObject a JSONObject with the weather data
     * @return an OwmForecast object, or null if the parsing fails
     */
    private OwmForecast parseForecast(JSONObject jsonObject) {
        try {
            JSONObject temps = jsonObject.getJSONObject("main");
            JSONObject weather = jsonObject.getJSONArray("weather").getJSONObject(0);

            long date = jsonObject.getLong("dt");
            double temp = temps.getDouble("temp");

            OwmForecast.WeatherState weatherState = stringToWeatherState(weather.getString("main"));

            // According to the OWM API, the temp_min and temp_max values in the "main" object
            // are optional, and that daily min/max temperatures are only contained in the paid
            // subscriptions.
            return new OwmForecast(date * 1000, temp, weatherState);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Converts a string represented weather state from OWM JSON into a WeatherState object.
     *
     * @param stateString state as a String as received from OWM
     * @return WeatherState object corresponding to the weather state
     */
    private OwmForecast.WeatherState stringToWeatherState(String stateString) {
        OwmForecast.WeatherState ret;

        switch (stateString) {
            // Rain and Snow both map to RAIN, as there's no separate icon for snow.
            case "Rain":
            case "Snow":
                ret = OwmForecast.WeatherState.RAIN;
                break;
            case "Clouds":
                ret = OwmForecast.WeatherState.CLOUDS;
                break;
            case "Storm":
                ret = OwmForecast.WeatherState.STORM;
                break;
            default:
                ret = OwmForecast.WeatherState.SUN;
        }

        Log.d(TAG, "stringToWeatherState() - stateString = '" + stateString + "', ret = " + ret);
        return ret;
    }

    List<OwmForecast> getForecasts() {
        return mForecasts;
    }

    String getCity() {
        return mCity;
    }
}
