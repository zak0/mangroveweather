package jaaska.jaakko.mangroveweather;

import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Class for handling the data retrieval from OpenWeatherMap.
 * This is a singleton.
 *
 * Created by jaakko on 05/12/2017.
 */

class OwmClient {

    private static final String TAG = "OwmClient";
    private static final String API_KEY = BuildConfig.OPEN_WEATHER_MAP_API_KEY;

    /** Current state of the client */
    private RequestState mRequestState = RequestState.IDLE;

    /** The data from the last successful OWM API query */
    private OwmData mOwmData;

    private final Object mOwmDataLock = new Object();
    private final Object mRequestStateLock = new Object();

    private static OwmClient sInstance;

    private OwmClient() {
        Log.d(TAG, "constructor()");
    }

    static OwmClient getInstance() {
        if (sInstance == null) {
            sInstance = new OwmClient();
        }
        return sInstance;
    }

    /**
     * Gets the weather from the OWM API for the given location for the given number of days.
     *
     * @param location location for the weather
     * @param units "metric" or "imperial"
     * @param days for how many days the forecast is requested
     */
    void getWeather(String location, String units, int days) {
        Resources res = MangroveWeatherApp.getContext().getResources();
        String requestUrlString = res.getString(R.string.owm_api_url,
                location,
                units,
                (days + 1) * 8, // Add 1 to day count, as first one is today.
                API_KEY);

        Log.d(TAG, "getWeather() - url = '" + requestUrlString + "'");

        // Only allow one request to be running at any given time.
        if (getRequestState() == RequestState.IDLE) {
            new OwmRequestAsyncTask().execute(requestUrlString);
        }
    }

    /**
     * Enumeration representing the state of the network requests to the OWM API.
     */
    private enum RequestState {
        IDLE,
        WORKING
    }

    private RequestState getRequestState() {
        synchronized (mRequestStateLock) {
            return mRequestState;
        }
    }

    /**
     * Updates the state of network activity, and does it thread-safely.
     *
     * @param newState current state
     */
    private void setRequestState(RequestState newState) {
        Log.d(TAG, "setRequestState() - newState = " + newState);
        synchronized (mRequestStateLock) {
            mRequestState = newState;
        }
    }

    String getCity() {
        synchronized (mOwmDataLock) {
            if (mOwmData == null) {
                return "---";
            }
            return mOwmData.getCity();
        }
    }

    List<OwmForecast> getForecasts() {
        synchronized (mOwmDataLock) {
            if (mOwmData == null) {
                return null;
            }
            return mOwmData.getForecasts();
        }
    }

    private void setOwmData(OwmData data) {
        synchronized (mOwmDataLock) {
            mOwmData = data;
        }
    }

    /**
     * AsyncTask that requests for the weather data from the OWM API, and parses the response into
     * a form that can be used in the UI.
     */
    private class OwmRequestAsyncTask extends AsyncTask<String, Void, Boolean> {

        private String mQueryResult;

        @Override
        protected Boolean doInBackground(String... urlStrings) {
            Log.d(TAG, "OwmRequestAsyncTask - new task started");
            setRequestState(RequestState.WORKING);
            boolean requestSucceeded = false;

            try {
                URL url = new URL(urlStrings[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.connect();

                InputStream inputStream = new BufferedInputStream(connection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                StringBuilder stringBuilder = new StringBuilder();
                String nextLine = reader.readLine();
                while (nextLine != null) {
                    Log.d(TAG, "OwmRequestAsyncTask - " + nextLine);
                    stringBuilder.append(nextLine);
                    nextLine = reader.readLine();
                }
                mQueryResult = stringBuilder.toString();

                requestSucceeded = true;

            } catch (IOException e) {
                e.printStackTrace();
            }

            return requestSucceeded;
        }

        @Override
        protected void onPostExecute(Boolean requestSucceeded) {
            // If the request succeeded (meaning we have a response), let's parse
            // it into a form that can be used in the UI.
            if (requestSucceeded) {
                try {
                    setOwmData(new OwmData(mQueryResult));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            Log.d(TAG, "OwmRequestAsyncTask - onPostExecute() - result = '" + mQueryResult + "'");
            setRequestState(RequestState.IDLE);

            // Request for a UI update.
            UiUpdateHandler.getInstance().obtainMessage().sendToTarget();
        }
    }

}
