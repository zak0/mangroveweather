package jaaska.jaakko.mangroveweather;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/**
 * A Handler that updates the UI.
 *
 * This is a singleton to ensure only one handler is running at any given time.
 *
 * Created by jaakko on 06/12/2017.
 */

public class UiUpdateHandler extends Handler {

    private DashboardActivity mDashboardActivity = null;

    private static UiUpdateHandler sInstance;

    private UiUpdateHandler() {
        // This need to run in the UI thread.
        super(Looper.getMainLooper());
    }

    static UiUpdateHandler getInstance() {
        if (sInstance == null) {
            sInstance = new UiUpdateHandler();
        }
        return sInstance;
    }


    // Marks the DashboardActivity to being available for UI update requests.
    void register(DashboardActivity dashboardActivity) {
        mDashboardActivity = dashboardActivity;
    }

    // Marks the DashboardActivity to not being available no more.
    void unRegister() {
        mDashboardActivity = null;
    }

    @Override
    public void handleMessage(Message msg) {
        if (mDashboardActivity != null) {
            mDashboardActivity.requestUiUpdate();
        }
    }

}
