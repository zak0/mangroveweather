package jaaska.jaakko.mangroveweather;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Activity for controlling the settings of the app.
 *
 * Created by jaakko on 07/12/2017.
 */

public class SettingsActivity extends AppCompatActivity {

    private SettingsRecyclerViewAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) { // It should never be null, but Studio gives a warning without the check.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        setTitle(R.string.settings);

        TextView versionView = findViewById(R.id.textViewVersion);
        String versionString = getResources().getString(R.string.app_version,
                BuildConfig.VERSION_NAME);

        versionView.setText(versionString);

        RecyclerView recyclerView = findViewById(R.id.recyclerViewSettings);
        mAdapter = new SettingsRecyclerViewAdapter();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

    }

    private class SettingsRecyclerViewAdapter extends RecyclerView.Adapter<SettingsRecyclerViewAdapter.ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_setting, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            viewHolder.textViewTitle = view.findViewById(R.id.textViewTitle);
            viewHolder.textViewValue = view.findViewById(R.id.textViewValue);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            // The settings have their static positions.
            int titleResId;
            String value;

            switch (position) {
                case Settings.SETTING_LOCATION:
                    titleResId = R.string.settings_location;
                    value = Settings.getLocation();
                    break;
                case Settings.SETTING_UNITS:
                    titleResId = R.string.settings_units;
                    // Display the unit in a user preferable format.
                    if (Settings.getUnits().equals("metric")) {
                        value = getString(R.string.celcius);
                    } else {
                        value = getString(R.string.fahrenheit);
                    }
                    break;
                case Settings.SETTING_DAYS:
                default:
                    titleResId = R.string.settings_days;
                    value = "" + Settings.getDays();
                    break;
            }

            holder.setting = position;
            holder.textViewTitle.setText(titleResId);
            holder.textViewValue.setText(value);
        }

        @Override
        public int getItemCount() {
            // There are always just three settings.
            return 3;
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            TextView textViewTitle;
            TextView textViewValue;
            int setting; // Which setting this ViewHolder is for.

            ViewHolder(View itemView) {
                super(itemView);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new SettingDialog(SettingsActivity.this, setting, new PositiveDialogAction() {
                            @Override
                            public void onPositiveAction() {
                                mAdapter.notifyDataSetChanged();
                            }
                        }).show();
                    }
                });
            }
        }
    }

}
