package jaaska.jaakko.mangroveweather;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class DashboardActivity extends AppCompatActivity {

    private ForecastsRecyclerViewAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the user settings. (Or set the defaults, if no settings are set)
        Settings.setLocation("Oulu");
        Settings.setUnits("metric");
        Settings.setDays(5);

        setContentView(R.layout.activity_dashboard);

        RecyclerView recyclerView = findViewById(R.id.recyclerViewForecast);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new ForecastsRecyclerViewAdapter();
        recyclerView.setAdapter(mAdapter);

        findViewById(R.id.imageInfo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(AboutActivity.class);
            }
        });

        findViewById(R.id.imageSettings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(SettingsActivity.class);
            }
        });
    }

    private void startActivity(Class targetActivity) {
        Intent intent = new Intent(DashboardActivity.this, targetActivity);
        startActivity(intent);
    }

    void requestUiUpdate() {
        // Refreshes the RecyclerView containing the forecast for upcoming days.
        mAdapter.notifyDataSetChanged();

        // Refreshes the header containing the weather for today.
        refreshHeader();
    }

    private void refreshHeader() {

        OwmForecast forecast;
        try {
            forecast = OwmClient.getInstance().getForecasts().get(0);
            if (forecast == null) {
                throw new NullPointerException("No forecast for today.");
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            return;
        }

        TextView textViewCity = findViewById(R.id.textViewLocation);
        TextView textViewWeather = findViewById(R.id.textViewWeather);
        TextView textViewTemperature = findViewById(R.id.textViewTemperature);
        TextView textViewLow = findViewById(R.id.textViewLow);
        TextView textViewHigh = findViewById(R.id.textViewHigh);
        TextView textViewDate = findViewById(R.id.textViewDate);
        ImageView imageViewWeatherIcon = findViewById(R.id.imageViewWeatherIcon);

        int weatherStrResId = R.string.clear;
        switch (forecast.getWeatherState()) {
            case RAIN:
                weatherStrResId = R.string.rain;
                break;
            case CLOUDS:
                weatherStrResId = R.string.clouds;
                break;
            case STORM:
                weatherStrResId = R.string.storm;
                break;
        }

        int weatherIconResId = getIconResourceIdForWeather(forecast.getWeatherState());

        SimpleDateFormat dayFormat = new SimpleDateFormat("E d MMM", getDefaultLocale());
        String dateString = dayFormat.format(forecast.getDate());

        Resources res = getResources();

        textViewCity.setText(OwmClient.getInstance().getCity());
        textViewWeather.setText(res.getString(weatherStrResId));
        textViewTemperature.setText(res.getString(R.string.temperature_value, forecast.getTemp()));
        textViewLow.setText(res.getString(R.string.daily_low, Math.round(forecast.getLowTemp())));
        textViewHigh.setText(res.getString(R.string.daily_high, Math.round(forecast.getHighTemp())));
        textViewDate.setText(dateString);
        imageViewWeatherIcon.setImageResource(weatherIconResId);
    }

    /**
     * Gets a corresponding icon resource ID for a weather state.
     *
     * @param weather WeatherState to get the icon for
     * @return ID of a icon resource
     */
    private int getIconResourceIdForWeather(OwmForecast.WeatherState weather) {
        switch (weather) {
            case RAIN:
                return R.drawable.ic_rain;
            case STORM:
                return R.drawable.ic_storm;
            case CLOUDS:
                return R.drawable.ic_clouds;
        }
        return R.drawable.ic_sun;
    }

    @Override
    protected void onResume() {
        super.onResume();
        OwmClient.getInstance().getWeather(Settings.getLocation(), Settings.getUnits(), Settings.getDays());
        UiUpdateHandler.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        UiUpdateHandler.getInstance().unRegister();
    }

    /**
     * Gets the default system locale.
     * The preferred method for retrieving this depends on the API level.
     * @return current default locale
     */
    private Locale getDefaultLocale() {
        // Get the default locale.
        Locale locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            locale = getResources().getConfiguration().getLocales().get(0);
        } else{
            locale = getResources().getConfiguration().locale;
        }
        return locale;
    }

    private class ForecastsRecyclerViewAdapter extends RecyclerView.Adapter<ForecastsRecyclerViewAdapter.ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_forecast, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);

            viewHolder.textViewDay = view.findViewById(R.id.textViewDay);
            viewHolder.textViewLowTemp = view.findViewById(R.id.textViewLow);
            viewHolder.textViewHighTemp = view.findViewById(R.id.textViewHigh);
            viewHolder.imageViewWeatherIcon = view.findViewById(R.id.imageViewWeatherIcon);

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            // Skip the first one, because the first forecast is the one for "today", which is
            // already displayed in the header.
            OwmForecast forecast = OwmClient.getInstance().getForecasts().get(position + 1);

            Resources res = getResources();
            String dateString = res.getString(R.string.tomorrow);

            // For days other than tomorrow, show the name of the day
            if (position > 0) {
                SimpleDateFormat dayFormat = new SimpleDateFormat("EEEE", getDefaultLocale());
                dateString = dayFormat.format(forecast.getDate());

                // Capitalize first letter of the day. Some locales (like Dutch) output
                // weekdays with lower case first letters.
                dateString = dateString.substring(0, 1).toUpperCase() + dateString.substring(1);
            }

            String dailyLow = res.getString(R.string.daily_low, Math.round(forecast.getLowTemp()));
            String dailyHigh = res.getString(R.string.daily_high, Math.round(forecast.getHighTemp()));

            holder.textViewDay.setText(dateString);
            holder.textViewLowTemp.setText(dailyLow);
            holder.textViewHighTemp.setText(dailyHigh);

            int iconResId = getIconResourceIdForWeather(forecast.getWeatherState());

            holder.imageViewWeatherIcon.setImageResource(iconResId);
        }

        @Override
        public int getItemCount() {
            List<OwmForecast> forecasts = OwmClient.getInstance().getForecasts();

            // Return size minus 1, because the first one is the one for "today", which is
            // already displayed in the header.
            return forecasts == null ? 0 : forecasts.size() - 1;
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            TextView textViewDay;
            TextView textViewLowTemp;
            TextView textViewHighTemp;
            ImageView imageViewWeatherIcon;

            ViewHolder(View view) {
                super(view);
            }
        }
    }
}
